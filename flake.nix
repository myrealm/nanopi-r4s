{
  description = "NanoPi R4S NixOS flake for myrealm";

  inputs = {
    # what does this even do?!
    nixpkgs = { url = "gitlab:myrealm/packages/main"; };
  };

  outputs = { nixpkgs, ... }@inputs:
  {
    nixosModules.default = { config, lib, ... }:
      with lib;
      let
        cfg = config.myrealm.nanopi;
      in
      {
        imports = [
          ./baseline.nix
          ./led.nix
        ];

        options.myrealm.nanopi = with types; {
          enable = mkEnableOption "myrealm settings for Nanopi R4S";
        };

        config = mkIf cfg.enable {
          myrealm.nanopi.nanopi-r4s-led.enable = true;
          myrealm.nanopi.nanopi-r4s-hw-baseline.enable = true;
        };
      };

    nixosModules = {

      nanopi-r4s-led = { config, lib, nixpkgs, ... }: with lib;
      {
        imports = [
          ./led.nix
        ];
      };

      nanopi-r4s-baseline = { config, lib, nixpkgs, ... }: with lib;
      {
        imports = [
          ./baseline.nix
        ];
      };

    };
  };
}
