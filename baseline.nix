{ lib, config, ... }:
with lib;
let
  cfg = config.myrealm.nanopi.nanopi-r4s-hw-baseline;
in
{

  options.myrealm.nanopi.nanopi-r4s-hw-baseline = with types; {
    enable = mkEnableOption "baseline settings";
  };

  config = mkIf cfg.enable {

    hardware.enableRedistributableFirmware = true;

    boot.loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = true;
    };

    boot.consoleLogLevel = lib.mkDefault 7;
    boot.kernelParams = ["cma=32M" "console=ttyS2,115200n8" "console=tty0"];

    #boot.kernelPackages = pkgs.linuxPackagesNanopiR4S;
  };
}
