{ lib, config, ... }:
with lib;
let
  cfg = config.myrealm.nanopi.nanopi-r4s-led;
in
{
  options.myrealm.nanopi.nanopi-r4s-led = with types; {
    enable = mkEnableOption "Enable settings for leds";

  };

  config = mkIf cfg.enable {

    boot = {
      kernelPatches = [{
        name = "led-trigger-netdev";
        patch = null;
        extraConfig = ''
          LEDS_TRIGGER_NETDEV y
        '';
      }];
    };

    networking.usePredictableInterfaceNames = false;

    services.udev.extraRules = ''
      DRIVERS=="rk_gmac-dwmac", NAME="wan"
      DRIVERS=="r8169", NAME="lan"
    '';

    systemd = {
      services.led-setup = {
        wantedBy = [ "multi-user.target" ];
        serviceConfig.Type = "oneshot";

        script = ''
          LAN_PREFIX="/sys/class/leds/green:lan"
          WAN_PREFIX="/sys/class/leds/green:wan"
          echo "heartbeat" > /sys/class/leds/red\:power/trigger

          echo "netdev" > $LAN_PREFIX/trigger
          echo "lan" >    $LAN_PREFIX/device_name
          echo "1" >      $LAN_PREFIX/rx
          echo "1" >      $LAN_PREFIX/tx
          echo "1" >      $LAN_PREFIX/link

          echo "netdev" > $WAN_PREFIX/trigger
          echo "wan" >    $WAN_PREFIX/device_name
          echo "1" >      $WAN_PREFIX/rx
          echo "1" >      $WAN_PREFIX/tx
          echo "1" >      $WAN_PREFIX/link
        '';
      };
    };
  };
}
