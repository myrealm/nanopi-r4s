{ pkgs, ... }:

{
  systemd = {
    services.heartbeat-led-setter = {
      wantedBy = [ "multi-user.target" ];
      serviceConfig.Type = "oneshot";
      script = ''
        echo "heartbeat" > /sys/class/leds/red\:power/trigger
        echo "heartbeat" > /sys/class/leds/green\:lan/trigger
        echo "heartbeat" > /sys/class/leds/green\:wan/trigger
      '';
    };
  };
}
